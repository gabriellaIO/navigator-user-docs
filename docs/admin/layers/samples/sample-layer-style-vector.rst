 Vector Layer Sample Config File
 ===============================
 
.. code-block: json
:linenos:

{
    "source": {
        "type": "vector",
        "scheme": "xyz",
        "tiles": [
          "https://data-gis.unep-wcmc.org/server/rest/services/Hosted/WDPA_Marine_Coastal/VectorTileServer/tile/{z}/{y}/{x}.pbf"
        ]
    },
    "applicationConfig": {
        "order": 0,
        "slug": "protected_areas"
      },
      "staticImageConfig": {},
    "maxzoom": 19,
    "minzoom": 2,

    "render": {
      "layers": 
      [
        {
          "source-layer": "WDPA_poly_latest",
          "type": "fill",
          "paint": { 
            "fill-opacity": 0.7 
          }
        },
        {
          "source-layer": "WDPA_poly_latest",
          "type": "line",
          "paint": {
            "line-opacity": 0,
            "line-width": 0
          }
        },
        {
          "source-layer": "WDPA_poly_latest",
          "filter": [
            "all",
            [
              "==",
              "iucn_cat",
              "II"
            ]
          ],
          "type": "fill",
          "paint": {
            "fill-color": "#7c549e"
          }
        },
        {
          "source-layer": "WDPA_poly_latest",
          "filter": [
            "all",
            [
              "==",
              "iucn_cat",
              "III"
            ]
          ],
          "type": "fill",
          "paint": {
            "fill-color": "#966db3"
          }
        },
        {
          "source-layer": "WDPA_poly_latest",
          "filter": [
            "all",
            [
              "==",
              "iucn_cat",
              "IV"
            ]
          ],
          "type": "fill",
          "paint": {
            "fill-color": "#b087c9"
          }
        },
        {
          "source-layer": "WDPA_poly_latest",
          "filter": [
            "all",
            [
              "==",
              "iucn_cat",
              "Ia"
            ]
          ],
          "type": "fill",
          "paint": {
            "fill-color": "#4a2574"
          }
        },
        {
          "source-layer": "WDPA_poly_latest",
          "filter": [
            "all",
            [
              "==",
              "iucn_cat",
              "Ib"
            ]
          ],
          "type": "fill",
          "paint": {
            "fill-color": "#633c89"
          }
        },
        {
          "source-layer": "WDPA_poly_latest",
          "filter": [
            "all",
            [
              "==",
              "iucn_cat",
              "Not Applicable"
            ]
          ],
          "type": "fill",
          "paint": {
            "fill-color": "#82347F"
          }
        },
        {
          "source-layer": "WDPA_poly_latest",
          "filter": [
            "all",
            [
              "==",
              "iucn_cat",
              "Not Assigned"
            ]
          ],
          "type": "fill",
          "paint": {
            "fill-color": "#82347F"
          }
        },
        {
          "source-layer": "WDPA_poly_latest",
          "filter": [
            "all",
            [
              "==",
              "iucn_cat",
              "Not Reported"
            ]
          ],
          "type": "fill",
          "paint": {
            "fill-color": "#82347F"
          }
        },
        {
          "source-layer": "WDPA_poly_latest",
          "filter": [
            "all",
            [
              "==",
              "iucn_cat",
              "V"
            ]
          ],
          "type": "fill",
          "paint": {
            "fill-color": "#caa1df"
          }
        },
        {
            "source-layer": "WDPA_poly_latest",
            "filter": [
                "all",
                [
                  "==",
                  "iucn_cat",
                  "VI"
                ]
            ],
            "type": "fill",
            "paint": {
                "fill-color": "#e5bcf6"
            }
        }
      ]
    },
    "legendConfig": {
      "items": [
        {
          "color": "#4a2574",
          "name": "Ia - strict nature reserve"
        },
        {
          "color": "#633c89",
          "name": "Ib - wildnerness area"
        },
        {
          "color": "#7c549e",
          "name": "II - national park"
        },
        {
          "color": "#966db3",
          "name": "III - national monument or feature"
        },
        {
          "color": "#b087c9",
          "name": "IV - habitat and species management area"
        },
        {
          "color": "#caa1df",
          "name": "V - protected landscape or seascape"
        },
        {
          "color": "#e5bcf6",
          "name": "VI - protected area with sustainable use of natural resources"
        },
        {
          "color": "#82347F",
          "name": "Uncategorized"
        }
      ],
      "type": "basic"
    },
    "interactionConfig": {
      "output": [
        {
          "column": "name",
          "format": null,
          "prefix": "",
          "property": "Name",
          "suffix": "",
          "type": "string"
        },
        {
          "column": "wdpaid",
          "format": null,
          "prefix": "",
          "property": "WDPA ID",
          "suffix": "",
          "type": "string"
        },
        {
          "column": "iucn_cat",
          "format": null,
          "prefix": "",
          "property": "IUCN Category",
          "suffix": "",
          "type": "string"
        },
        {
          "column": "status",
          "format": null,
          "prefix": "",
          "property": "Status",
          "suffix": "",
          "type": "string"
        },
        {
          "column": "status_yr",
          "format": null,
          "prefix": "",
          "property": "Status year",
          "suffix": "",
          "type": "string"
        },
        {
          "column": "desig",
          "format": null,
          "prefix": "",
          "property": "Designation",
          "suffix": "",
          "type": "string"
        },
        {
          "column": "own_type",
          "format": null,
          "prefix": "",
          "property": "Ownership Type",
          "suffix": "",
          "type": "string"
        },
        {
          "column": "gov_type",
          "format": null,
          "prefix": "",
          "property": "Governance Type",
          "suffix": "",
          "type": "string"
        },
        {
          "column": "mang_auth",
          "format": null,
          "prefix": "",
          "property": "Management Authority",
          "suffix": "",
          "type": "string"
        },
        {
          "column": "gis_area",
          "format": ",.0f",
          "prefix": "",
          "property": "Area",
          "suffix": "km²",
          "type": "number"
        }
      ]
    }
  }
