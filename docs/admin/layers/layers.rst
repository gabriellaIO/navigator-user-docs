Adding an existing STAC layer
=============================

..toctree ::
  :name: Layer Styling
  :caption: Layer Styling
  
  layer_styling

The layer config has three main sections

* source

    * sets asset type (raster, vector, ...) 
    * the location of the tiles
    * sets the min and max zoom levels

* STACConfig

    * the STAC server and the asset
    * the tiler service to be used
    * the data type (catagorical or continous)

* legendConfig

    * defines how layer should look onscreen
    * can determine which vector data appears in each grouping

The config file can be created manually, using the Source panel, or a combination of the two.

Layer Source Panel
------------------

The Layer Source panel consists of the following sections.

STAC API URL

Collection

Asset

Data Type

Date Range

Map Tiles

Zoom Levels

Layer Styling



