===================================
IO Navigator Platform Documentation 
===================================

.. image:: images/Spinning_globe.png
   :width: 600 px
   :alt: Spinning globe. 

**Leaders in governments, NGOs, and industry need trustworthy,
actionable information about the changing world to set strategies,
measure the impacts of actions, and report progress against plans.**

**IO Navigator empowers these leaders with AI-powered data and metrics
delivering science-based insights and automated monitoring in an
intuitive, easy-to-use software platform.**

Create and Share Dashboards 📊
	- Explore over 100 datasets to monitor the status of past and future actions
Analyze Data 🕵️		
	- Indicators automatically calculate and visualize critical environmental trends over custom places
	- Compare your places to IO’s catalogue of 4,900 standard places 
	- Download indicator results as CSV or JSON files 
	- Clip and export raster data 
Work With Your Team 👥
	- GIS experience not required 
	- Cloud Hosted - access from anywhere, anytime
	- Screenshot Indicator visualizations for your reports
	- Embed maps into your website
Secure and Private 🔐
	- Private Workspace for your team 
	- Control your team’s user permissions with four access levels 

This user guide has been developed to walk you through key tools and functions of your private workspace on the IO Navigator Platform. 

.. toctree::
   :maxdepth: 2
   :caption: Access Your Account:
   
   accounts/accounts

.. toctree::
   :maxdepth: 2
   :caption: Getting Started:
   
   mapview/map

.. toctree::
   :maxdepth: 2
   :caption: Explore Environmental Data:
   
   indicators/data-indicators

.. toctree::
   :maxdepth: 2
   :caption: Run Custom Analysis:
   
   workspaces/workspaces
   collections/collections

.. toctree::
   :maxdepth: 2
   :caption: Managing Content:

   content/datalayers

.. toctree::
   :maxdepth: 2
   :caption: Learn More:
   
   contact/contact-us
   contribute/style-guide
   glossary/glossary

.. toctree::
   :maxdepth: 2
   :caption: Trouble Shooting

   troubles/troubleshooting

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
