.. glossary:

Glossary
========

.. glossary::

  admin
    An admin is a user with the highest permission level. An admin has
    viewer and editor permissions, can add and manage users, and
    assign roles to users as editors and viewers.

  AOI
    Area of Interest

  AOR
    Area of Responsibility

  basemap
    The basemap refers to the map style of the map view. The basemap
    can be set to a grayscale map style or a satellite basemap from
    Mapbox. No specific dates on specific imagery tiles can be
    provided by Mapbox at this time. Satellite basemap images are from
    a variety of satellites and range from a few years to a few months
    old.

  clip and export
    The clip and export layers button can be used to download raster
    data from a global dataset over a specific place from the platform
    to a user's local machine. Data downloads are in geoTIFF format.

  collection
    A collection is a user-defined group of places. Places in a
    collection must all be within a single workspace. Collections
    allow you to quickly download indicator results from a set of
    places.

  dashboards
    A dashboard is a set of widgets or indicators that have been
    selected to be visualized together on the map view. Widgets must
    be added to a dashboard within the same workspace to be able to
    visualize

  editor
    An editor is a user with all viewer permissions and can manage
    assets via the admin tool. Editors do not have access to add or
    remove users. While not required, experience working with GIS or
    design software will better enable editors when uploading and
    removing places, editing layer legend styles and colors in the
    layer config box, and editing layer and place metadata, such as
    title, filters, and description boxes

  indicator
    Navigator automatically calculates indicators or statistical
    measurements over eight datasets for any place on the platform.
    Indicator calculations are used to extract data driven insights
    about regions of the world using global datasets.

  Layer
    A Layer is a GeoTIFF or Vector file that used to store science
    data or satellite imagery, along with geographic metadata that
    geolocates the location.

  map view
    The map view refers to the world map on Navigator. The map view
    can be manipulated using the controls in the button right corner.
    The map view is where users can search for Places or Layers to
    extract insights from environmental data for any area of the
    world.

  Navigator
    Navigator is the name of the Impact Observatory geospatial
    platform.

  place
    A place is a .geojson file that represents any given location on
    Earth. The Navigator workspace contains over 4,000 places
    representing all countries and regions of the world. Within a
    private workspace, users with editor permissions can upload custom
    places. All metrics within a given workspace are automatically
    calculated for every place within that workspace.

  place type
    Place type is a user-defined category for a place. Place type is a
    required field when creating a place within the Navigator admin.
    The place type selection does not affect the place data; however,
    the place type will appear next to the place name in the places
    search results.

  spatial resolution
    This refers to the size of an individual pixel in the image and is
    related to the smallest feature that can be detected when viewing
    the imagery. There is no consensus concerning spatial resolution
    classes or nomenclature. What might be "high resolution" in one
    application area may well be "low" in another.

  slug
    A human readable unique identifier for a place or layer. Each
    place and layer on the platform has a unique slug name and a
    unique ID number. Slug names can be automatically generated when
    creating a new place or layer by clicking GENERATE A SLUG NAME.
    Slug names can also be customized by typing in the slug box or
    edited after a shape has been created.

  user
    A user refers to a single account on the Navigator platform.

  viewer
    A viewer is a user with the lowest permission level. A viewer can
    view all workspace assets, including places and layers, on the map
    view. Viewers can also view and download indicator results, clip
    and export data from a layer for any place, create collections,
    clip and export data from a collection. Viewers do not have access
    to the admin tool from where assets and users can be added,
    edited, and removed.

  widget
    A widget is the same thing as an indicator. See definition for
    indicator.

  workspace
    A workspace provides a secure work area within the Navigator
    platform where your organization’s data (places, layers, widgets,
    dashboards & users) can be added and shared with a set of
    specified users.

  workspace admin
    The workspace admin is a secure place where your organization’s
    data can be stored. The workspace admin is where users can upload
    custom places and manage other users’ permissions. The workspace
    admin is available to users with editor, admin, or owner
    permissions. To access the workspace admin, click on the MAP VIEW
    button to open a drop down menu, select the ADMIN button to the
    right of the workspace name.
