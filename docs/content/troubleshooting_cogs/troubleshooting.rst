STAC Entries Tool 
=================

The STAC Entries tool enables you to create STAC collections, items, and assets using flexible JSON files with links to your geospatial assets. To create layers in Navigator, geospatial data in Cloud Optimized GeoTIFF (COG) format must be indexed in STAC to connect data to backend platform services. The STAC Entries tool in the workspace admin enables users with editor or admin permissions to create and manage new raster layers on the Navigator platform.

.. tip:: Raster data can be linked to Navigator through other providers as long as they have a publicly available tiling service, such as Google Earth Engine or the Planetary Computer. 

.. tip:: Vector datasets are rendered without STAC. Use ESRI to connect vector datasets. 


What is STAC?
-------------

“The STAC specification is a common language to describe geospatial information, so it can more easily be worked with, indexed, and discovered. Users of spatial temporal data are often burdened with building unique pipelines for each different collection of data they consume. The STAC community has defined this specification to remove this complexity and spur common tooling.” See the `STAC spec website <https://stacspec.org/en>`_ for more background on STAC (STAC Spec, 2022).

Navigator queries a STAC Catalog available at `io-stac-navigator.impactobservatory.com <https://io-stac-navigator.impactobservatory.com>`_. We recommend using a JSON viewer plug in. The IO STAC Browser is a more user-friendly way to view the STAC. 

STAC is a relatively new technology. Currently, collections in IO's STAC link to geospatial data stored in Microsoft Azure, however, in the future it will be possible to upload data in any loation using the STAC Entries tool. The vision for STAC is to create a standardized method of cataloging and exposing data, “so that new code doesn't need to be written whenever a new data set or API is released.” Geospatial data stored in any cloud can be indexed in the STAC, but IO limits users to Azure at this time. In the future layers on Navigator could be supported from a variety of locations. In addition, Navigator could pull from other providers’ [publicly available] STAC Catalogs, so that data does not need to be moved around . 

The following steps are intended to guide you through the process of creating a layer from a GeoTIFF, using just one of many possible methods. See this map of possible STAC routes to envision how STAC could be used in combination with Navigator in the future . 


STAC Entries Tool: From GeoTIFF to Layer in Navigator
------------------------------------------------------

Overview
--------
1. Upload Cloud Optimized GeoTIFF(s) to an Azure storage container
2. Create STAC collection, items and assets in Navigator 
3. Create a new layer in Navigator to query the STAC Collection 


You can create a layer in Navigator from a GeoTIFF or set of GeoTIFFs. Although not required, you will benefit from familiarity with Microsoft Azure, STAC, and GIS data formats. 

Step 1. Create Valid Cloud Optimizes GeoTIFFs (COGs) 
----------------------------------------------------
Layers on Navigator must be stored in Cloud Optimized GeoTIFF (COG) format. A Cloud Optimized GeoTIFF or COG is a regular GeoTIFF file, aimed at being hosted on an HTTP file server, with an internal organization that enables more efficient workflows on the cloud. 

For more information about COGs, including troublshooting COG creation, see `More about COG files <https://docs.impactobservatory.com/content/troubleshooting_cogs/troubleshooting.html>`_ 

.. tip:: Files that are not in valid COG format are not accepted into the STAC Catalog by the STAC Entries tool. 

Default COG command

 .. code-block:: bash

gdal_translate -of COG -co COMPRESS=DEFLATE -co OVERVIEW_RESAMPLING={METHOD} src_tif dst_cog

 .. code-block:: bash

*``CO OVERVIEW_RESAMPLING`` needs to be set based on the type of data (with ``-of COG`` this will default to ``CUBIC`` unless there’s an internal colormap, then it is ``NEAREST``). Set ``METHOD`` to ``NEAREST`` for categorical data, and ``AVERAGE`` for continuous data. (``-co OVERVIEW_RESAMPLING NEAREST``)

*Add ``-co BIGTIFF YES`` if input filesize > 4GB. Do not rely on ``-co BIGTIFF IF_NEEDED``.

*Try ``-co NUM_THREADS=ALL_CPUS`` to enable multithreading compression and overviews.

*If needed, sometimes ``DEFLATE`` can be faster for decompressing/serving tiles.

.. tip:: Check out the `COG Driver Documentation <https://gdal.org/drivers/raster/cog.html>`_.



Step 2. Upload COG Files to Azure 
---------------------------------
A Shared Access Signature (SAS) key is required to gain access to upload data to Impact Observatory hosted Azure Containers. Please email IO if you need an SAS key to a container. Once you have valid COG files and an SAS key, the command line or can be used to upload files to Azure. if needed, download azcopy. For more information see, the `azcopy documnetation <https://learn.microsoft.com/en-us/azure/storage/common/storage-use-azcopy-blobs-upload?toc=%2Fazure%2Fstorage%2Fblobs%2Ftoc.json>`_. 

With the azcopy copy command, use the following syntax to upload the file(s)to an Azure container:
 
.. code-block:: bash
azcopy copy '<local-file-path>' 'https://<storage-account-name>.<blob or dfs>.core.windows.net/<container-name>/<blob-name>'
.. code-block:: bash


Step 3. Use the STAC Entries Tool to Create a STAC Collection 
--------------------------------------------------------------

Once your data is in Azure, use the STAC Entries tool to create STAC infrastructure to enable your data to become a layer. You must have editor or admin permissions to access the STAC Entries tool in Navigator. Open the admin for your workspace. From the Home dropdown menu select STAC Entries, then Create New STAC Entry.

The STAC Entries tool contains two Graphical User Interfaces (GUIs) to create JSON files with links to your files in Azure. As a user, you need to replace the fields in the template JSON provided with the relevant metadata for your files. The first GUI contains a template to create a new STAC Collection. If you have an existing STAC Collection you would like to add data to, select **I would like to use an existing STAC Collection.** Then provide the exact name of your STAC collection and select **Link to STAC Collection.** 

If your data is not related to any existing STAC Collections you need to create a new STAC Collection. Begin by typing in the GUI to edit the JSON. Use the template provided to replace the required and optional STAC properties.

.. code-block:: bash
{
  "host": { "host": "io-stac-navigator.impactobservatory.com" },
  "collection_id": "",
  "spatial_extent": { "bbox": [ -180, -90, 180, 180 ] },
  "temporal_extent": { "start": "2022-01-01", "end": "2022-12-31" },
  "description": "",
  "license": "proprietary",
  "provider": { "name": "example", "url": "http://example.com" },
  "properties": {}
}
.. code-block:: bash


Keep in mind these properties should apply to all of the COG files that will be in the STAC Collection. If some data have different properties, that is a good sign to use separate STAC Collections for each category of data. 
STAC Property

**Text to Replace or Add**

Property Definition

host

Do not replace "https://io-stac-navigator.impactobservatory.com"

The host is the URL to the destination STAC Catalog. Do not replace this URL. 

collection_id

"io-collection-name-here"

The collection ID is the name of the collection. Replace with a name of your choosing. No spaces. 

spatial_extent

[-180.0, -90.0, 180.0, 90.0] 

The spatial extent is a geographic bounding box, or the smallest rectangle that will completely enclose all data that will be in the collection. Use the format MaxX, MaxY, MinX, and MinY.  -180.0, -90.0, 180.0, 90.0 is used for a global dataset. One way to find the bounding box is to open the properties of the data in QGIS.

temporal_extent

"2021-01-01" and "2022-12-31"

The date range for the collection. Replace with dates that encompass the entire time span of all the data that will be in the collection. If there are multiple years of data, the start date should be the oldest date and the end date should be the most recent date. Use a full year for annual data, for example, if the data is from 2020 only, replace the dates with 2020-01-01 and 2021-12-31.

description

"short description"

Optional to include a short description of all the data in the collection. 

license

"proprietary"

Optional to provide a license for the data in the collection. 

provider

"provider name" and "href to provider"

Optional to provide the name and URL of your organization, the provider, and/or the source.

properties

"misc_field_1": and "misc_value_1"

Optional to create custom properties to add to the Collection. For example, you could create a property called  "misc_field_1": to store the styling information, such as "io:style": "categorical". 



``host`` The host is the URL to the destination STAC Catalog. The default should be kept at https://io-stac-navigator.impactobservatory.com

``collection_id`` The collection ID is the name of the collection. Replace 'unbl-collection-name-here' with a name of your choosing. No spaces. 

``spatial_extent`` The spatial extent is a geographic bounding box, or the smallest rectangle that will completely enclose all data that will be in the collection. Use the format MaxX, MaxY, MinX, and MinY. The bounding box -180.0, -90.0, 180.0, 90.0 is used for a global dataset. ..tip: Open data in QGIS and view the properties to copy and paste the bounding box. 

``temporal_extent`` The date range for the collection. Replace with dates that encompass the entire time span of all the data that will be in the collection. If there are multiple years of data, the start date should be the oldest date and the end date should be the most recent date. Use a full year for annual data, for example, if the data is from 2020 only, replace the dates with 2020-01-01 and 2021-12-31.

``description`` Optional to include a short description of all the data in the collection. 

``license`` Optional to provide a license for the data in the collection. 

``provider`` Optional to provide the name and URL of your organization, the provider, and/or the source. 

``properties`` Optional to create custom properties to add to the Collection. For example, you could replace  'misc_field_1' with a property to store the styling information, such as 'io:style': categorical.

Once the STAC properties are defined, click **Create STAC Collection**. 



Troubleshooting
---------------

A red dot to the left of a line indicates an error in the JSON. You will not be able to submit your JSON until you fix the error. Common JSON errors include missing or extra commas, parentheses, or quotes. 



Step 3. Create STAC Item(s) and Asset(s) 
----------------------------------------

The second GUI in the STAC Entires tool contains a template JSON to create items and assets to add to the previously specified STAC Collection. 

.. tip:: STAC Organization 
The number of items or assets in your collection depends on your data's spatial and temporal variances. You can decide the structure they want to use for the items and assets. The general recommendation to organize the structure of your STAC collection is to use multiple assets for spatially and temporally identical data/files (assets need to match in time and geographic extent). Use different items for data/files that differ temporally and geographically. Use different collections if there’s a clear logical difference in the data (ie. different source data, a different version of a dataset, etc.).


The files must be valid Cloud Optimized GeoTIFFs. 

The template JSON provides fields for multiple items and multiple assets within those items. You can edit this structure according to the temporal and spatial extents of their dataset(s). If there is variation in the temporal or spatial extent of the files, they typically should be organized into multiple items with single assets.

.. code-block:: bash
[
  {
    "item_id": "",
    "assets": {
      "asset_1": "/vsiaz/href_to_cog_file.tif"
    },
    "item_date": "2022-01-01",
    "properties": {}
  }
]
.. code-block:: bash

``item_id`` The name of the item. Replace ``item_id``  with a new name or keep the name in the template. Item names can be repeated or unique. 

``assets`` The asset(s) within an item are where links to COG files should be provided. 

    ``asset_1`` The name of the asset. Optional to replace ``asset_1`` with a more descriptive name for your data. The asset name will be used by the navigator to query the files for a layer from a collection. If you would like your Layer in Navigator to pull multiple COG files into a single layer (such as a tiled dataset) make sure to give all assets throughout multiple items the same name. Replace ``vsiaz/url/to/cog_file.tif`` with the file path to your COG file in Azure. Should start with ``/vsiaz`` and end with ``.tif``. Optional to include additional assets within the same item or to create separate items each with their own assets. Read more on STAC organization here. 

    ``item_date`` The date for the files linked in the item. Required to replace ``2022-01-01`` with year, month and day for your data. Only one date is permitted, therefore all assets in an item should have the same date. Navigator will use this date range to query the Collection for the proper date range for your layer. For example, if a collection contains 10 years of data organized into 10 items, Navigator can create individual layers for each year by querying STAC for a date range within the collection, as defined by the assets. 

``properties`` Optional to create custom properties for each item to record additional metadata in STAC. 



Once all properties are defined, click **Save**. If successful, a message will appear with a link to the layers admin in your workspace. Click on the link if you would like to create a new layer. 

If the JSON is not accepted into the STAC, an error message will return.


Step 4. Create New Layer from STAC
----------------------------------

You can create a new layer from a STAC Collection in the Layers section of the admin. Select Create New Layer, provide a name, and select Layer provider → IO STAC API. 

Next provide fields to query the STAC for the specific spatial and temporal assets you would like to include in your layer: 
