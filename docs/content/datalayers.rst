Configuring Datalayers using the Navigator Admin
================================================
 
How to configure a datalayer on the Navigator platform.
 
Pre-requisites:
 
* Original dataset should be a GeoTiff file
* Python environment and GDAL installed - used for cog conversion
* Access to Azure Blob Storage to host the file
* Available STAC instance - persistence layers for assets
* Available TiTiler instance - used for generating map tiles, rendered on the map view in Navigator
* User must have "editor" permissions in Navigator to access the admin
 
Step 1. Convert the GeoTiff to a Cloud Optimised GeoTiff (COG)
--------------------------------------------------------------
 
The entire ecosystem of Navigator relies on working with Cloud Optimised GeoTiffs (COGs).
 
.. _gdal_translate: https://gdal.org/programs/gdal_translate.html
 
GDAL offers the utility function `gdal_translate`_ to convert a GeoTiff to a COG:
 
.. code-block:: shell
 
   gdal_translate PATH_TO_GEO_TIFF OUTPUT_FILE -of COG -co COMPRESS=LZW -co BIGTIFF=IF_NEEDED
 
The resulting COG file should be uploaded to a container in Azure Blob Storage, where it can be referenced by the STAC.
 
 
.. image:: images/nav_layer_azure_storage_explorer.png
  :width: 600 px
  :alt: Azure Storage Exloper
 
 
 
Step 2. Preparing a STAC Collection
-----------------------------------
The most common STAC structure used by IO for Navigator is to create a single collection per science dataset and designate COG URL(s) as STAC items within that collection. This structure allows for assets with the same metadata (excluding space and time differences) to be a part of the same collection. Before uploading the COG to the STAC, you need to prepare a STAC
Collection, a simple flexible JSON file that provides a structure to organize
and browse one or multiple STAC Items.
 
A STAC collection has additional information such as the ``extents``, ``license``, ``keywords``,
``providers``, etc that describe STAC Items that fall within the Collection. For example:
 
* **id:** unbl-marine-pollution-index-rescaled
* **title:** unbl-marine-pollution-index-rescaled
* **license:**  CC BY 1.0
* **spatial extent:**  ``[-179.999998694099,-57.964209813775255,179.99944550757888,84.99582458608734]``
* **temporal extent:**  ``2013-01-01T00:00:00.000Z - 2014-01-01T00:00:00.000Z``
 
Note: the spatial and temporal extent of the collection should be equal to the reunion of all the assets in the collection.
 
:doc:`Here’s an example of details stored for a STAC Collection <datalayer_example>`
 
The COG will be uploaded to the STAC using a script.
 
Step 3. Creating a STAC based layer on the Navigator Platform
-------------------------------------------------------------
 
 
Once the STAC collection and items are configured, you are ready to add the layer to Navigator. The user must have "editor" permissions. To access the layer manager in the admin:
 
- Click on Map View to open a drop-down menu of your workspaces
- Select ADMIN for the workspace in which you would like to add the layer
- Once you are in the admin of the workspace, click on Home to open a drop-down menu
- Select Layers
- Click Create a New Layer 
 
You need to provide the following information to point the platform to a STAC-based Layer:
 
* **Layer Provider:** IO STAC Service (this will update the UI with STAC specific fields)
* **STAC API URL:** the root URL for your STAC API instance, backed by a STAC PostgreSQL database 
* **Collection:** the ID of the collection where you uploaded the STAC item
* **Asset:**  the name of the COG asset in the STAC Collection
* **Date range:** of the cog asset (specified in the properties when creating the STAC item)
* **Tiler URL:** A Titiler instance URL from where Navigator can generate tiles
* **Data Type:** Select continuous data to create a custom style or categorical to use the categories included in the STAC metadata
* **Rescale:** the min and max data values for the COG that you want to style (e.q. 0 - 32)
* **Layer styling:**
  * Allows you to configure coloring breakpoints
  * These breakpoints **must have a value between 0 and 1**
  * These values are directly mapped by the tiler to the range you provided for the rescale min and max, for example:
  * 0 <-- 0
  * 1 <-- 32
 
  * The Name for each value is required as it is used to label the legend. Use a space bar to leave the name blank, or click the eye button to hide the value AND the color from the legend.
 
You can use this `Data Range Normalizer Tool <https://docs.google.com/spreadsheets/d/1-6r_C4kc9G8WExd8m_urkZPmeadNYxQJr6pdwPQRxPA/edit#gid=0>`_ (google sheet) to help calculate the value between 0 and 1 from the actual range of values. (Duplicate to your personal google drive.)
 
.. image:: images/nav_layer_add_panel.png
  :width: 600 px
  :alt: Layer sources and meta-data
 
 
.. image:: images/nav_layer_rescale.png
  :width: 600 px
  :alt: Creating a Color Map
 
 
.. image:: images/nav_layer_stac_styling.png
  :width: 600 px
  :alt: Styled Layer from STAC meta-data
 
 
Click save, then turn on the Publish toggle to see the layer appear in the map view.

.. toctree ::
  :name: Datalayer Example

  datalayer_example

.. toctree ::
  :name: Troubleshooting COGs

  troubleshooting_cogs/troubleshooting

.. toctree:: 
  :name: Datalayer Catalogs
  
  datalayer_catalog
