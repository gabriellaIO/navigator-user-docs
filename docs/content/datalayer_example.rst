.. _datalayer_example:

Example of how we store an entry for a stac collection
------------------------------------------------------

Collection Name: UNBL Marine Pollution Index

.. code-block:: javascript

    // 20211001092006
    // http://io-stac-navigator.azurewebsites.net/collections/unbl-marine-pollution-index/items/unbl-marine-pollution-index_plumes-fert-combo

    {
        "id": "unbl-marine-pollution-index_plumes-fert-combo",
        "bbox": [
            -180.0,
            -89.144,
            180.00000000000006,
            90.0
        ],
        "type": "Feature",
        "links": [
            {
            "rel": "collection",
            "type": "application/json",
            "href": "http://io-stac-navigator.azurewebsites.net/collections/unbl-marine-pollution-index"
            },
            {
            "rel": "parent",
            "type": "application/json",
            "href": "http://io-stac-navigator.azurewebsites.net/collections/unbl-marine-pollution-index"
            },
            {
            "rel": "root",
            "type": "application/json",
            "href": "http://io-stac-navigator.azurewebsites.net/"
            },
            {
            "rel": "self",
            "type": "application/geo+json",
            "href": "http://io-stac-navigator.azurewebsites.net/collections/unbl-marine-pollution-index/items/unbl-marine-pollution-index_plumes-fert-combo"
            },
            {
            "rel": "alternate",
            "type": "application/json",
            "title": "tiles",
            "href": "http://io-stac-navigator.azurewebsites.net/collections/unbl-marine-pollution-index/items/unbl-marine-pollution-index_plumes-fert-combo/tiles"
            }
        ],
        "assets": {
            "composite": {
            "href": "/vsiaz/io-platform-dev/platform_layers/unbl/plumes_fert_combo_cog.tif",
            "type": "image/tiff; application=geotiff; profile=cloud-optimized"
            }
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
            [
                [
                180.00000000000006,
                -89.144
                ],
                [
                180.00000000000006,
                90.0
                ],
                [
                -180.0,
                90.0
                ],
                [
                -180.0,
                -89.144
                ],
                [
                180.00000000000006,
                -89.144
                ]
            ]
            ]
        },
        "collection": "unbl-marine-pollution-index",
        "properties": {
            "io:crs": "EPSG:4326",
            "datetime": "2013-01-01T00:00:00Z",
            "io:range": [
            0.0,
            1.4
            ],
            "io:nodata": 0.0,
            "end_datetime": "2014-01-01T00:00:00Z",
            "io:data_type": "continuous",
            "start_datetime": "2013-01-01T00:00:00Z"
        },
        "stac_version": "1.0.0",
        "stac_extensions": [
            
        ]
    }

